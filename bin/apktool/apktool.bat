@echo off

set TYPE=%1

REM %~1 会去掉两边的引号，%1则不会
if %TYPE%==d (
    goto :decode
) else if %TYPE%==b (
    goto :build
) else (
    goto :err
)

REM 用于展示参数详情，有需就用
:args
echo 1 = %1
echo 2 = %2
echo 3 = %3
echo 4 = %4
echo 5 = %5
echo 6 = %6
echo 7 = %7
echo 8 = %8
echo 9 = %9
goto :end

REM 反编译：只反编译主dex们
:decode
java -jar "%~dp0\apktool\apktool2.jar" %TYPE% --only-main-classes %2 %3 %4 %5 %6 %7 %8 %9
goto :end

REM 编译：调整默认为aapt
:build
java -jar "%~dp0\apktool\apktool2.jar" %TYPE% --use-aapt1         %2 %3 %4 %5 %6 %7 %8 %9
goto :end

:err
echo E：无效参数，参数1必须为 'd' 或 'b'，现在是：%TYPE%

:end