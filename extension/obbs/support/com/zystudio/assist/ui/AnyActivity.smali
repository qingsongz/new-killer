.class public Lcom/zystudio/assist/ui/AnyActivity;
.super Landroid/app/Activity;
.source "AnyActivity.java"


# instance fields
.field private mRootView:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private addRootView()V
    .locals 3

    .line 48
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/zystudio/assist/ui/AnyActivity;->mRootView:Landroid/widget/RelativeLayout;

    .line 49
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 52
    iget-object v2, p0, Lcom/zystudio/assist/ui/AnyActivity;->mRootView:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 53
    iget-object v1, p0, Lcom/zystudio/assist/ui/AnyActivity;->mRootView:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v1, v0}, Lcom/zystudio/assist/ui/AnyActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public enableGame()V
    .locals 1

    .line 77
    invoke-static {}, Lcom/zystudio/assist/logic/WorkOfGame;->get()Lcom/zystudio/assist/logic/WorkOfGame;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zystudio/assist/logic/WorkOfGame;->legalStartGame()V

    .line 78
    invoke-virtual {p0}, Lcom/zystudio/assist/ui/AnyActivity;->finish()V

    return-void
.end method

.method public getRootView()Landroid/widget/RelativeLayout;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/zystudio/assist/ui/AnyActivity;->mRootView:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public isHorizontal()Z
    .locals 2

    .line 72
    invoke-virtual {p0}, Lcom/zystudio/assist/ui/AnyActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 32
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    .line 33
    invoke-virtual {p0, v0}, Lcom/zystudio/assist/ui/AnyActivity;->requestWindowFeature(I)Z

    if-eqz p1, :cond_0

    .line 35
    invoke-virtual {p0}, Lcom/zystudio/assist/ui/AnyActivity;->finish()V

    return-void

    .line 38
    :cond_0
    invoke-direct {p0}, Lcom/zystudio/assist/ui/AnyActivity;->addRootView()V

    .line 40
    invoke-static {}, Lcom/zystudio/assist/logic/WorkOfGame;->get()Lcom/zystudio/assist/logic/WorkOfGame;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/zystudio/assist/logic/WorkOfGame;->init(Landroid/content/Context;)V

    .line 41
    invoke-static {}, Lcom/zystudio/assist/tools/SpHelper;->get()Lcom/zystudio/assist/tools/SpHelper;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/zystudio/assist/tools/SpHelper;->init(Landroid/content/Context;)V

    .line 42
    new-instance p1, Lcom/zystudio/assist/logic/WorkFlow;

    invoke-direct {p1, p0}, Lcom/zystudio/assist/logic/WorkFlow;-><init>(Lcom/zystudio/assist/ui/AnyActivity;)V

    invoke-virtual {p1}, Lcom/zystudio/assist/logic/WorkFlow;->chooseFlow()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 44
    invoke-virtual {p0}, Lcom/zystudio/assist/ui/AnyActivity;->enableGame()V

    :cond_1
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .line 63
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 64
    invoke-static {}, Lcom/zystudio/assist/logic/WorkOfObb;->get()Lcom/zystudio/assist/logic/WorkOfObb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zystudio/assist/logic/WorkOfObb;->closeTask()V

    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 0

    .line 58
    invoke-static {}, Lcom/zystudio/assist/permission/ObbPermission;->get()Lcom/zystudio/assist/permission/ObbPermission;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/zystudio/assist/permission/ObbPermission;->onRequestPermissionsResult(I)V

    return-void
.end method
