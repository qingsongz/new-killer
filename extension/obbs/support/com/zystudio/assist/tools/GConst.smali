.class public final Lcom/zystudio/assist/tools/GConst;
.super Ljava/lang/Object;
.source "GConst.java"


# static fields
.field public static final DIR_BASE:Ljava/lang/String; = "yzyz"

.field public static final DIR_DATAS:Ljava/lang/String; = "satad"

.field public static final DIR_DEX:Ljava/lang/String; = "sxed"

.field public static final DIR_OBB:Ljava/lang/String; = "5dd0"

.field public static final DIR_TXT:Ljava/lang/String; = "txts"

.field public static final DIR_UNITY:Ljava/lang/String; = "ytinu"

.field public static final DOT:C = '.'

.field public static final OBB_FILE_NAME_PATTERN:Ljava/util/regex/Pattern;

.field public static final OBB_SUFFIX:Ljava/lang/String; = ".obb"

.field public static final ZyActivity:Ljava/lang/String; = "com.zystudio.dev.ZyActivity"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "^(main|patch)\\.\\d+\\..+\\.obb$"

    .line 28
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/zystudio/assist/tools/GConst;->OBB_FILE_NAME_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "yzyz"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
