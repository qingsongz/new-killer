.class public final Lcom/zystudio/assist/logic/WorkOfObb;
.super Ljava/lang/Object;
.source "WorkOfObb.java"


# static fields
.field private static instance:Lcom/zystudio/assist/logic/WorkOfObb;


# instance fields
.field private anyActivity:Lcom/zystudio/assist/ui/AnyActivity;

.field private final copyState:Lcom/zystudio/assist/interf/ICopyState;

.field private copyTask:Lcom/zystudio/assist/logic/CopyTask;

.field private isHengPing:Z

.field private obbView:Lcom/zystudio/assist/ui/ObbView;

.field private final permissionCallback:Lcom/zystudio/assist/permission/IPermissionCallback;


# direct methods
.method public static synthetic $r8$lambda$Qkre6OjeFZC8bQWuc-ugQ0ofwfY(Lcom/zystudio/assist/logic/WorkOfObb;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/zystudio/assist/logic/WorkOfObb;->startObbFlow(Z)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Lcom/zystudio/assist/logic/WorkOfObb$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/zystudio/assist/logic/WorkOfObb$$ExternalSyntheticLambda0;-><init>(Lcom/zystudio/assist/logic/WorkOfObb;)V

    iput-object v0, p0, Lcom/zystudio/assist/logic/WorkOfObb;->permissionCallback:Lcom/zystudio/assist/permission/IPermissionCallback;

    .line 25
    new-instance v0, Lcom/zystudio/assist/logic/WorkOfObb$1;

    invoke-direct {v0, p0}, Lcom/zystudio/assist/logic/WorkOfObb$1;-><init>(Lcom/zystudio/assist/logic/WorkOfObb;)V

    iput-object v0, p0, Lcom/zystudio/assist/logic/WorkOfObb;->copyState:Lcom/zystudio/assist/interf/ICopyState;

    return-void
.end method

.method static synthetic access$000(Lcom/zystudio/assist/logic/WorkOfObb;)Lcom/zystudio/assist/ui/ObbView;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/zystudio/assist/logic/WorkOfObb;->obbView:Lcom/zystudio/assist/ui/ObbView;

    return-object p0
.end method

.method static synthetic access$100(Lcom/zystudio/assist/logic/WorkOfObb;)Z
    .locals 0

    .line 15
    iget-boolean p0, p0, Lcom/zystudio/assist/logic/WorkOfObb;->isHengPing:Z

    return p0
.end method

.method static synthetic access$200(Lcom/zystudio/assist/logic/WorkOfObb;)Lcom/zystudio/assist/ui/AnyActivity;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/zystudio/assist/logic/WorkOfObb;->anyActivity:Lcom/zystudio/assist/ui/AnyActivity;

    return-object p0
.end method

.method public static get()Lcom/zystudio/assist/logic/WorkOfObb;
    .locals 1

    .line 57
    sget-object v0, Lcom/zystudio/assist/logic/WorkOfObb;->instance:Lcom/zystudio/assist/logic/WorkOfObb;

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Lcom/zystudio/assist/logic/WorkOfObb;

    invoke-direct {v0}, Lcom/zystudio/assist/logic/WorkOfObb;-><init>()V

    sput-object v0, Lcom/zystudio/assist/logic/WorkOfObb;->instance:Lcom/zystudio/assist/logic/WorkOfObb;

    .line 60
    :cond_0
    sget-object v0, Lcom/zystudio/assist/logic/WorkOfObb;->instance:Lcom/zystudio/assist/logic/WorkOfObb;

    return-object v0
.end method

.method private setContentView()V
    .locals 3

    .line 86
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 89
    new-instance v1, Lcom/zystudio/assist/ui/ObbView;

    iget-object v2, p0, Lcom/zystudio/assist/logic/WorkOfObb;->anyActivity:Lcom/zystudio/assist/ui/AnyActivity;

    invoke-direct {v1, v2}, Lcom/zystudio/assist/ui/ObbView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/zystudio/assist/logic/WorkOfObb;->obbView:Lcom/zystudio/assist/ui/ObbView;

    .line 90
    iget-object v1, p0, Lcom/zystudio/assist/logic/WorkOfObb;->anyActivity:Lcom/zystudio/assist/ui/AnyActivity;

    invoke-virtual {v1}, Lcom/zystudio/assist/ui/AnyActivity;->getRootView()Landroid/widget/RelativeLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/zystudio/assist/logic/WorkOfObb;->obbView:Lcom/zystudio/assist/ui/ObbView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private startObbFlow(Z)V
    .locals 2

    if-nez p1, :cond_0

    const-string p1, "\u68c0\u6d4b\u5230\u7f3a\u5c11\u91ca\u653e\u8d44\u6e90\u7684\u5fc5\u8981\u6743\u9650\uff01\n\u8bf7\u624b\u52a8\u6253\u5f00\u201c\u5b58\u50a8\u6743\u9650\u201d\uff0c\u7136\u540e\u91cd\u542f\u6e38\u620f\u3002\n\u6216\u8005\u91cd\u65b0\u8fdb\u5165\u6e38\u620f\uff0c\u540c\u610f\u6743\u9650\u3002"

    .line 106
    iget-object v0, p0, Lcom/zystudio/assist/logic/WorkOfObb;->obbView:Lcom/zystudio/assist/ui/ObbView;

    invoke-virtual {v0, p1}, Lcom/zystudio/assist/ui/ObbView;->showDesc(Ljava/lang/String;)V

    return-void

    .line 109
    :cond_0
    invoke-static {}, Lcom/zystudio/assist/logic/ObbSDK;->getInstance()Lcom/zystudio/assist/logic/ObbSDK;

    move-result-object p1

    invoke-virtual {p1}, Lcom/zystudio/assist/logic/ObbSDK;->hasRoom()Z

    move-result p1

    if-nez p1, :cond_1

    const-string p1, "\u5f53\u524d\u5b58\u50a8\u7a7a\u95f4\u4e0d\u8db3\uff0c\u8bf7\u5148\u6e05\u7406\u624b\u673a\u7a7a\u95f4 ~~"

    .line 111
    iget-object v0, p0, Lcom/zystudio/assist/logic/WorkOfObb;->obbView:Lcom/zystudio/assist/ui/ObbView;

    invoke-virtual {v0, p1}, Lcom/zystudio/assist/ui/ObbView;->showDesc(Ljava/lang/String;)V

    return-void

    .line 114
    :cond_1
    invoke-static {}, Lcom/zystudio/assist/logic/ObbSDK;->getInstance()Lcom/zystudio/assist/logic/ObbSDK;

    move-result-object p1

    invoke-virtual {p1}, Lcom/zystudio/assist/logic/ObbSDK;->getSaveObbDir()Ljava/lang/String;

    move-result-object p1

    .line 115
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 116
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p1

    if-nez p1, :cond_2

    .line 117
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result p1

    if-nez p1, :cond_2

    const-string p1, "\u8d44\u6e90\u91ca\u653e\u5931\u8d25"

    .line 120
    iget-object v0, p0, Lcom/zystudio/assist/logic/WorkOfObb;->obbView:Lcom/zystudio/assist/ui/ObbView;

    invoke-virtual {v0, p1}, Lcom/zystudio/assist/ui/ObbView;->showDesc(Ljava/lang/String;)V

    return-void

    .line 124
    :cond_2
    new-instance p1, Lcom/zystudio/assist/logic/CopyTask;

    iget-object v0, p0, Lcom/zystudio/assist/logic/WorkOfObb;->anyActivity:Lcom/zystudio/assist/ui/AnyActivity;

    iget-object v1, p0, Lcom/zystudio/assist/logic/WorkOfObb;->copyState:Lcom/zystudio/assist/interf/ICopyState;

    invoke-direct {p1, v0, v1}, Lcom/zystudio/assist/logic/CopyTask;-><init>(Landroid/content/Context;Lcom/zystudio/assist/interf/ICopyState;)V

    iput-object p1, p0, Lcom/zystudio/assist/logic/WorkOfObb;->copyTask:Lcom/zystudio/assist/logic/CopyTask;

    .line 125
    invoke-virtual {p1}, Lcom/zystudio/assist/logic/CopyTask;->execute()V

    return-void
.end method

.method private startObbLogic()V
    .locals 2

    .line 94
    invoke-static {}, Lcom/zystudio/assist/permission/ObbPermission;->get()Lcom/zystudio/assist/permission/ObbPermission;

    move-result-object v0

    iget-object v1, p0, Lcom/zystudio/assist/logic/WorkOfObb;->anyActivity:Lcom/zystudio/assist/ui/AnyActivity;

    invoke-virtual {v0, v1}, Lcom/zystudio/assist/permission/ObbPermission;->init(Landroid/app/Activity;)V

    .line 95
    invoke-static {}, Lcom/zystudio/assist/permission/ObbPermission;->get()Lcom/zystudio/assist/permission/ObbPermission;

    move-result-object v0

    iget-object v1, p0, Lcom/zystudio/assist/logic/WorkOfObb;->permissionCallback:Lcom/zystudio/assist/permission/IPermissionCallback;

    invoke-virtual {v0, v1}, Lcom/zystudio/assist/permission/ObbPermission;->setPermissionCallback(Lcom/zystudio/assist/permission/IPermissionCallback;)V

    .line 96
    invoke-static {}, Lcom/zystudio/assist/permission/ObbPermission;->get()Lcom/zystudio/assist/permission/ObbPermission;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zystudio/assist/permission/ObbPermission;->start()V

    return-void
.end method


# virtual methods
.method public closeTask()V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/zystudio/assist/logic/WorkOfObb;->copyTask:Lcom/zystudio/assist/logic/CopyTask;

    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {v0}, Lcom/zystudio/assist/logic/CopyTask;->shutdown()V

    :cond_0
    return-void
.end method

.method public startUp(Lcom/zystudio/assist/ui/AnyActivity;)V
    .locals 0

    .line 66
    iput-object p1, p0, Lcom/zystudio/assist/logic/WorkOfObb;->anyActivity:Lcom/zystudio/assist/ui/AnyActivity;

    .line 68
    invoke-virtual {p1}, Lcom/zystudio/assist/ui/AnyActivity;->isHorizontal()Z

    move-result p1

    iput-boolean p1, p0, Lcom/zystudio/assist/logic/WorkOfObb;->isHengPing:Z

    .line 69
    invoke-static {}, Lcom/zystudio/assist/logic/ObbSDK;->getInstance()Lcom/zystudio/assist/logic/ObbSDK;

    move-result-object p1

    invoke-virtual {p1}, Lcom/zystudio/assist/logic/ObbSDK;->isObbFlowEnable()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 70
    invoke-direct {p0}, Lcom/zystudio/assist/logic/WorkOfObb;->setContentView()V

    .line 71
    invoke-direct {p0}, Lcom/zystudio/assist/logic/WorkOfObb;->startObbLogic()V

    goto :goto_0

    .line 73
    :cond_0
    iget-object p1, p0, Lcom/zystudio/assist/logic/WorkOfObb;->anyActivity:Lcom/zystudio/assist/ui/AnyActivity;

    invoke-virtual {p1}, Lcom/zystudio/assist/ui/AnyActivity;->enableGame()V

    :goto_0
    return-void
.end method
