.class public final Lcom/zystudio/assist/logic/WorkFlow;
.super Ljava/lang/Object;
.source "WorkFlow.java"


# instance fields
.field private final mActivity:Lcom/zystudio/assist/ui/AnyActivity;


# direct methods
.method public static synthetic $r8$lambda$wZpgNyXbEPN49Qb3Vzk-fNMecHY(Lcom/zystudio/assist/logic/WorkFlow;)V
    .locals 0

    invoke-direct {p0}, Lcom/zystudio/assist/logic/WorkFlow;->copyObb()V

    return-void
.end method

.method public constructor <init>(Lcom/zystudio/assist/ui/AnyActivity;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/zystudio/assist/logic/WorkFlow;->mActivity:Lcom/zystudio/assist/ui/AnyActivity;

    return-void
.end method

.method private copyObb()V
    .locals 2

    const-string v0, "5dd0"

    .line 139
    invoke-static {v0}, Lcom/zystudio/assist/tools/GConst;->getPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 140
    invoke-direct {p0, v0}, Lcom/zystudio/assist/logic/WorkFlow;->getDirFiles(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 142
    invoke-static {}, Lcom/zystudio/assist/logic/WorkOfObb;->get()Lcom/zystudio/assist/logic/WorkOfObb;

    move-result-object v0

    iget-object v1, p0, Lcom/zystudio/assist/logic/WorkFlow;->mActivity:Lcom/zystudio/assist/ui/AnyActivity;

    invoke-virtual {v0, v1}, Lcom/zystudio/assist/logic/WorkOfObb;->startUp(Lcom/zystudio/assist/ui/AnyActivity;)V

    :cond_0
    return-void
.end method

.method private copyToDataFolder(Ljava/lang/String;)V
    .locals 2

    .line 129
    invoke-static {p1}, Lcom/zystudio/assist/tools/GConst;->getPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 131
    :try_start_0
    iget-object v1, p0, Lcom/zystudio/assist/logic/WorkFlow;->mActivity:Lcom/zystudio/assist/ui/AnyActivity;

    invoke-static {v1, v0, p1}, Lcom/zystudio/assist/logic/WorkOfData;->copyAssetsToAppDir(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 133
    :catch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "copyToDataFolder "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/zystudio/assist/tools/ZyLog;->showError(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private copyUnity()V
    .locals 3

    const-string v0, "ytinu"

    .line 119
    invoke-static {v0}, Lcom/zystudio/assist/tools/GConst;->getPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 121
    :try_start_0
    iget-object v1, p0, Lcom/zystudio/assist/logic/WorkFlow;->mActivity:Lcom/zystudio/assist/ui/AnyActivity;

    invoke-static {v1, v0}, Lcom/zystudio/assist/logic/WorkOfData;->copyAssetsToSdcardDir(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 123
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "copyUnity "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/zystudio/assist/tools/ZyLog;->showError(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private getDirFiles(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2

    .line 109
    :try_start_0
    iget-object v0, p0, Lcom/zystudio/assist/logic/WorkFlow;->mActivity:Lcom/zystudio/assist/ui/AnyActivity;

    invoke-virtual {v0}, Lcom/zystudio/assist/ui/AnyActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "chooseFlow "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/zystudio/assist/tools/ZyLog;->showError(Ljava/lang/String;)V

    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private showCopyUI()V
    .locals 4

    .line 86
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/zystudio/assist/logic/WorkFlow;->mActivity:Lcom/zystudio/assist/ui/AnyActivity;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const-string v1, "\u6e38\u620f\u8d44\u6e90\u91ca\u653e\u4e2d..."

    .line 87
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/high16 v1, -0x1000000

    .line 88
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    const/high16 v1, 0x41a00000    # 20.0f

    .line 89
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    const/4 v1, -0x1

    .line 90
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    const/16 v2, 0x11

    .line 91
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 92
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x2

    invoke-direct {v2, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v3, 0xd

    .line 95
    invoke-virtual {v2, v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 96
    iget-object v1, p0, Lcom/zystudio/assist/logic/WorkFlow;->mActivity:Lcom/zystudio/assist/ui/AnyActivity;

    invoke-virtual {v1}, Lcom/zystudio/assist/ui/AnyActivity;->getRootView()Landroid/widget/RelativeLayout;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method beginCopyPatch([Ljava/lang/String;)V
    .locals 8

    .line 50
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    const/4 v4, 0x1

    if-ge v2, v0, :cond_4

    aget-object v5, p1, v2

    .line 51
    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v6

    const/4 v7, -0x1

    sparse-switch v6, :sswitch_data_0

    goto :goto_1

    :sswitch_0
    const-string v6, "ytinu"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    goto :goto_1

    :cond_0
    const/4 v7, 0x3

    goto :goto_1

    :sswitch_1
    const-string v6, "txts"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    goto :goto_1

    :cond_1
    const/4 v7, 0x2

    goto :goto_1

    :sswitch_2
    const-string v6, "sxed"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    goto :goto_1

    :cond_2
    const/4 v7, 0x1

    goto :goto_1

    :sswitch_3
    const-string v6, "5dd0"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    goto :goto_1

    :cond_3
    const/4 v7, 0x0

    :goto_1
    packed-switch v7, :pswitch_data_0

    .line 65
    invoke-direct {p0, v5}, Lcom/zystudio/assist/logic/WorkFlow;->copyToDataFolder(Ljava/lang/String;)V

    goto :goto_2

    .line 60
    :pswitch_0
    invoke-direct {p0}, Lcom/zystudio/assist/logic/WorkFlow;->copyUnity()V

    .line 61
    invoke-static {}, Lcom/zystudio/assist/tools/SpHelper;->get()Lcom/zystudio/assist/tools/SpHelper;

    move-result-object v5

    const-string v6, "k2"

    invoke-virtual {v5, v6, v4}, Lcom/zystudio/assist/tools/SpHelper;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_2

    :pswitch_1
    const/4 v3, 0x1

    :goto_2
    :pswitch_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 70
    :cond_4
    invoke-static {}, Lcom/zystudio/assist/tools/SpHelper;->get()Lcom/zystudio/assist/tools/SpHelper;

    move-result-object p1

    const-string v0, "k3"

    invoke-virtual {p1, v0, v4}, Lcom/zystudio/assist/tools/SpHelper;->putBoolean(Ljava/lang/String;Z)V

    if-eqz v3, :cond_5

    .line 74
    iget-object p1, p0, Lcom/zystudio/assist/logic/WorkFlow;->mActivity:Lcom/zystudio/assist/ui/AnyActivity;

    new-instance v0, Lcom/zystudio/assist/logic/WorkFlow$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/zystudio/assist/logic/WorkFlow$$ExternalSyntheticLambda1;-><init>(Lcom/zystudio/assist/logic/WorkFlow;)V

    invoke-virtual {p1, v0}, Lcom/zystudio/assist/ui/AnyActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_3

    .line 76
    :cond_5
    iget-object p1, p0, Lcom/zystudio/assist/logic/WorkFlow;->mActivity:Lcom/zystudio/assist/ui/AnyActivity;

    invoke-virtual {p1}, Lcom/zystudio/assist/ui/AnyActivity;->enableGame()V

    .line 79
    :goto_3
    invoke-static {}, Lcom/zystudio/assist/tools/SpHelper;->get()Lcom/zystudio/assist/tools/SpHelper;

    move-result-object p1

    const-string v0, "k1"

    invoke-virtual {p1, v0, v4}, Lcom/zystudio/assist/tools/SpHelper;->putBoolean(Ljava/lang/String;Z)V

    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x199b5b -> :sswitch_3
        0x3615c4 -> :sswitch_2
        0x368c03 -> :sswitch_1
        0x6df6ef5 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public chooseFlow()Z
    .locals 3

    const-string v0, "yzyz"

    .line 27
    invoke-direct {p0, v0}, Lcom/zystudio/assist/logic/WorkFlow;->getDirFiles(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    .line 28
    array-length v2, v0

    if-ge v2, v1, :cond_0

    goto :goto_0

    .line 33
    :cond_0
    invoke-static {}, Lcom/zystudio/assist/tools/SpHelper;->get()Lcom/zystudio/assist/tools/SpHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/zystudio/assist/tools/SpHelper;->hasCopyAllFile()Z

    move-result v2

    if-eqz v2, :cond_1

    return v1

    .line 38
    :cond_1
    invoke-direct {p0}, Lcom/zystudio/assist/logic/WorkFlow;->showCopyUI()V

    .line 40
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/zystudio/assist/logic/WorkFlow$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0, v0}, Lcom/zystudio/assist/logic/WorkFlow$$ExternalSyntheticLambda0;-><init>(Lcom/zystudio/assist/logic/WorkFlow;[Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    const/4 v0, 0x0

    return v0

    :cond_2
    :goto_0
    return v1
.end method

.method synthetic lambda$chooseFlow$0$com-zystudio-assist-logic-WorkFlow([Ljava/lang/String;)V
    .locals 0

    .line 40
    invoke-virtual {p0, p1}, Lcom/zystudio/assist/logic/WorkFlow;->beginCopyPatch([Ljava/lang/String;)V

    return-void
.end method
