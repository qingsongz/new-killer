.class public final Lcom/zystudio/assist/permission/ObbPermission;
.super Ljava/lang/Object;
.source "ObbPermission.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zystudio/assist/permission/ObbPermission$SingleTon;
    }
.end annotation


# static fields
.field private static final CODE:I = 0x3e9

.field private static final permissions:[Ljava/lang/String;


# instance fields
.field private mActivityReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private mCallback:Lcom/zystudio/assist/permission/IPermissionCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "android.permission.WRITE_EXTERNAL_STORAGE"

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    .line 19
    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zystudio/assist/permission/ObbPermission;->permissions:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get()Lcom/zystudio/assist/permission/ObbPermission;
    .locals 1

    .line 31
    invoke-static {}, Lcom/zystudio/assist/permission/ObbPermission$SingleTon;->access$000()Lcom/zystudio/assist/permission/ObbPermission;

    move-result-object v0

    return-object v0
.end method

.method private isGranted(Ljava/lang/String;)Z
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/zystudio/assist/permission/ObbPermission;->mActivityReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0, p1}, Landroidx/core/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public init(Landroid/app/Activity;)V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/zystudio/assist/permission/ObbPermission;->mActivityReference:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/zystudio/assist/permission/ObbPermission;->mActivityReference:Ljava/lang/ref/WeakReference;

    :cond_0
    return-void
.end method

.method public onRequestPermissionsResult(I)V
    .locals 1

    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_0

    .line 93
    iget-object p1, p0, Lcom/zystudio/assist/permission/ObbPermission;->mCallback:Lcom/zystudio/assist/permission/IPermissionCallback;

    invoke-virtual {p0}, Lcom/zystudio/assist/permission/ObbPermission;->verify()Z

    move-result v0

    invoke-interface {p1, v0}, Lcom/zystudio/assist/permission/IPermissionCallback;->callback(Z)V

    :cond_0
    return-void
.end method

.method public setPermissionCallback(Lcom/zystudio/assist/permission/IPermissionCallback;)V
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/zystudio/assist/permission/ObbPermission;->mCallback:Lcom/zystudio/assist/permission/IPermissionCallback;

    return-void
.end method

.method public start()V
    .locals 3

    .line 67
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1e

    const/4 v2, 0x1

    if-ge v0, v1, :cond_0

    .line 68
    iget-object v0, p0, Lcom/zystudio/assist/permission/ObbPermission;->mCallback:Lcom/zystudio/assist/permission/IPermissionCallback;

    invoke-interface {v0, v2}, Lcom/zystudio/assist/permission/IPermissionCallback;->callback(Z)V

    return-void

    .line 71
    :cond_0
    invoke-virtual {p0}, Lcom/zystudio/assist/permission/ObbPermission;->verify()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/zystudio/assist/permission/ObbPermission;->mCallback:Lcom/zystudio/assist/permission/IPermissionCallback;

    invoke-interface {v0, v2}, Lcom/zystudio/assist/permission/IPermissionCallback;->callback(Z)V

    return-void

    .line 75
    :cond_1
    iget-object v0, p0, Lcom/zystudio/assist/permission/ObbPermission;->mActivityReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v1, 0x21

    if-lt v0, v1, :cond_2

    .line 76
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.MANAGE_APP_ALL_FILES_ACCESS_PERMISSION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 77
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "package:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/zystudio/assist/permission/ObbPermission;->mActivityReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 78
    iget-object v1, p0, Lcom/zystudio/assist/permission/ObbPermission;->mActivityReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 79
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    const/4 v0, 0x0

    .line 80
    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    return-void

    .line 83
    :cond_2
    iget-object v0, p0, Lcom/zystudio/assist/permission/ObbPermission;->mActivityReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    sget-object v1, Lcom/zystudio/assist/permission/ObbPermission;->permissions:[Ljava/lang/String;

    const/16 v2, 0x3e9

    invoke-static {v0, v1, v2}, Landroidx/core/app/ActivityCompat;->requestPermissions(Landroid/app/Activity;[Ljava/lang/String;I)V

    return-void
.end method

.method public verify()Z
    .locals 5

    .line 49
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/zystudio/assist/permission/ObbPermission;->mActivityReference:Ljava/lang/ref/WeakReference;

    .line 50
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v1, 0x21

    if-lt v0, v1, :cond_0

    .line 51
    invoke-static {}, Landroid/os/Environment;->isExternalStorageManager()Z

    move-result v0

    return v0

    .line 53
    :cond_0
    sget-object v0, Lcom/zystudio/assist/permission/ObbPermission;->permissions:[Ljava/lang/String;

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, v0, v3

    .line 54
    invoke-direct {p0, v4}, Lcom/zystudio/assist/permission/ObbPermission;->isGranted(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    return v2

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    return v0
.end method
