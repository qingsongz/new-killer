.class public final Lcom/zystudio/assist/logic/WorkOfGame;
.super Ljava/lang/Object;
.source "WorkOfGame.java"


# static fields
.field private static instance:Lcom/zystudio/assist/logic/WorkOfGame;


# instance fields
.field private mContext:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get()Lcom/zystudio/assist/logic/WorkOfGame;
    .locals 1

    .line 18
    sget-object v0, Lcom/zystudio/assist/logic/WorkOfGame;->instance:Lcom/zystudio/assist/logic/WorkOfGame;

    if-nez v0, :cond_0

    .line 19
    new-instance v0, Lcom/zystudio/assist/logic/WorkOfGame;

    invoke-direct {v0}, Lcom/zystudio/assist/logic/WorkOfGame;-><init>()V

    sput-object v0, Lcom/zystudio/assist/logic/WorkOfGame;->instance:Lcom/zystudio/assist/logic/WorkOfGame;

    .line 21
    :cond_0
    sget-object v0, Lcom/zystudio/assist/logic/WorkOfGame;->instance:Lcom/zystudio/assist/logic/WorkOfGame;

    return-object v0
.end method

.method private hasAdSdk()Z
    .locals 3

    .line 50
    invoke-virtual {p0}, Lcom/zystudio/assist/logic/WorkOfGame;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "com.zystudio.dev.ZyActivity"

    .line 52
    invoke-static {v2, v1, v0}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :catch_0
    return v1
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/zystudio/assist/logic/WorkOfGame;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    return-object v0
.end method

.method public init(Landroid/content/Context;)V
    .locals 1

    .line 25
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/zystudio/assist/logic/WorkOfGame;->mContext:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public legalStartGame()V
    .locals 3

    .line 33
    invoke-static {}, Lcom/zystudio/assist/ObbConfig;->get()Lcom/zystudio/assist/ObbConfig;

    move-result-object v0

    invoke-direct {p0}, Lcom/zystudio/assist/logic/WorkOfGame;->hasAdSdk()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/zystudio/assist/ObbConfig;->gameLauncherName(Z)Ljava/lang/String;

    move-result-object v0

    .line 34
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/zystudio/assist/logic/WorkOfGame;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "android.intent.action.MAIN"

    .line 36
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "android.intent.category.LAUNCHER"

    .line 37
    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 38
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 39
    invoke-virtual {p0}, Lcom/zystudio/assist/logic/WorkOfGame;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
