.class Lcom/zystudio/assist/logic/WorkOfObb$1;
.super Ljava/lang/Object;
.source "WorkOfObb.java"

# interfaces
.implements Lcom/zystudio/assist/interf/ICopyState;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zystudio/assist/logic/WorkOfObb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zystudio/assist/logic/WorkOfObb;


# direct methods
.method constructor <init>(Lcom/zystudio/assist/logic/WorkOfObb;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/zystudio/assist/logic/WorkOfObb$1;->this$0:Lcom/zystudio/assist/logic/WorkOfObb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCopyEnd()V
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/zystudio/assist/logic/WorkOfObb$1;->this$0:Lcom/zystudio/assist/logic/WorkOfObb;

    invoke-static {v0}, Lcom/zystudio/assist/logic/WorkOfObb;->access$000(Lcom/zystudio/assist/logic/WorkOfObb;)Lcom/zystudio/assist/ui/ObbView;

    move-result-object v0

    const-string v1, "\u5373\u5c06\u8fdb\u5165\u6e38\u620f..."

    invoke-virtual {v0, v1}, Lcom/zystudio/assist/ui/ObbView;->showBigHint(Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/zystudio/assist/logic/WorkOfObb$1;->this$0:Lcom/zystudio/assist/logic/WorkOfObb;

    invoke-static {v0}, Lcom/zystudio/assist/logic/WorkOfObb;->access$200(Lcom/zystudio/assist/logic/WorkOfObb;)Lcom/zystudio/assist/ui/AnyActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zystudio/assist/ui/AnyActivity;->enableGame()V

    return-void
.end method

.method public onCopyFail(Ljava/lang/String;)V
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/zystudio/assist/logic/WorkOfObb$1;->this$0:Lcom/zystudio/assist/logic/WorkOfObb;

    invoke-static {v0}, Lcom/zystudio/assist/logic/WorkOfObb;->access$000(Lcom/zystudio/assist/logic/WorkOfObb;)Lcom/zystudio/assist/ui/ObbView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/zystudio/assist/ui/ObbView;->showBigHint(Ljava/lang/String;)V

    return-void
.end method

.method public onCopyStart(IIJ)V
    .locals 4

    .line 34
    iget-object v0, p0, Lcom/zystudio/assist/logic/WorkOfObb$1;->this$0:Lcom/zystudio/assist/logic/WorkOfObb;

    invoke-static {v0}, Lcom/zystudio/assist/logic/WorkOfObb;->access$100(Lcom/zystudio/assist/logic/WorkOfObb;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "\u6b63\u5728\u91ca\u653e\u8d44\u6e90\uff1a%d/%d\t\u5f53\u524d\u8d44\u6e90\uff1a%s"

    goto :goto_0

    :cond_0
    const-string v0, "\u6b63\u5728\u91ca\u653e\u8d44\u6e90\uff1a%d/%d\n\u5f53\u524d\u8d44\u6e90\uff1a%s"

    :goto_0
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x1

    add-int/2addr p1, v2

    .line 39
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v3, 0x0

    aput-object p1, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v1, v2

    const/4 p1, 0x2

    invoke-static {p3, p4}, Lcom/zystudio/assist/tools/ComUtil;->sizeFormat(J)Ljava/lang/String;

    move-result-object p2

    aput-object p2, v1, p1

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 40
    iget-object p2, p0, Lcom/zystudio/assist/logic/WorkOfObb$1;->this$0:Lcom/zystudio/assist/logic/WorkOfObb;

    invoke-static {p2}, Lcom/zystudio/assist/logic/WorkOfObb;->access$000(Lcom/zystudio/assist/logic/WorkOfObb;)Lcom/zystudio/assist/ui/ObbView;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/zystudio/assist/ui/ObbView;->showDesc(Ljava/lang/String;)V

    return-void
.end method

.method public onCopying(I)V
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/zystudio/assist/logic/WorkOfObb$1;->this$0:Lcom/zystudio/assist/logic/WorkOfObb;

    invoke-static {v0}, Lcom/zystudio/assist/logic/WorkOfObb;->access$000(Lcom/zystudio/assist/logic/WorkOfObb;)Lcom/zystudio/assist/ui/ObbView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "%"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/zystudio/assist/ui/ObbView;->showBigHint(Ljava/lang/String;)V

    return-void
.end method
