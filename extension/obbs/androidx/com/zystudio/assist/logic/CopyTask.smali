.class public final Lcom/zystudio/assist/logic/CopyTask;
.super Ljava/lang/Object;
.source "CopyTask.java"


# instance fields
.field private final THREAD_POOL_EXECUTOR:Ljava/util/concurrent/ExecutorService;

.field private final context:Landroid/content/Context;

.field private copyProcess:I

.field private copySize:J

.field private final copyState:Lcom/zystudio/assist/interf/ICopyState;

.field private final handler:Landroid/os/Handler;

.field private totalSize:J


# direct methods
.method public static synthetic $r8$lambda$N-p7eXhqwWbFLL0W23okxQr8vAE(Lcom/zystudio/assist/logic/CopyTask;)V
    .locals 0

    invoke-direct {p0}, Lcom/zystudio/assist/logic/CopyTask;->launchCopyTask()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/zystudio/assist/interf/ICopyState;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/zystudio/assist/logic/CopyTask;->context:Landroid/content/Context;

    .line 33
    iput-object p2, p0, Lcom/zystudio/assist/logic/CopyTask;->copyState:Lcom/zystudio/assist/interf/ICopyState;

    .line 34
    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p2

    invoke-direct {p1, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/zystudio/assist/logic/CopyTask;->handler:Landroid/os/Handler;

    const/4 p1, 0x1

    .line 35
    invoke-static {p1}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object p1

    iput-object p1, p0, Lcom/zystudio/assist/logic/CopyTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method private copyLogic(IILjava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "5dd0"

    invoke-static {v1}, Lcom/zystudio/assist/tools/GConst;->getPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 83
    iget-object v1, p0, Lcom/zystudio/assist/logic/CopyTask;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 84
    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v1

    int-to-long v6, v1

    .line 85
    new-instance v1, Lcom/zystudio/assist/logic/CopyTask$$ExternalSyntheticLambda1;

    move-object v2, v1

    move-object v3, p0

    move v4, p1

    move v5, p2

    invoke-direct/range {v2 .. v7}, Lcom/zystudio/assist/logic/CopyTask$$ExternalSyntheticLambda1;-><init>(Lcom/zystudio/assist/logic/CopyTask;IIJ)V

    invoke-virtual {p0, v1}, Lcom/zystudio/assist/logic/CopyTask;->runOnUI(Ljava/lang/Runnable;)V

    .line 87
    new-instance p1, Ljava/io/File;

    invoke-static {}, Lcom/zystudio/assist/logic/ObbSDK;->getInstance()Lcom/zystudio/assist/logic/ObbSDK;

    move-result-object p2

    invoke-virtual {p2}, Lcom/zystudio/assist/logic/ObbSDK;->getSaveObbDir()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 88
    new-instance p2, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 89
    new-instance p1, Ljava/io/FileOutputStream;

    invoke-direct {p1, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/16 p2, 0x1000

    new-array p2, p2, [B

    .line 93
    :cond_0
    :goto_0
    invoke-virtual {v0, p2}, Ljava/io/InputStream;->read([B)I

    move-result p3

    const/4 v1, 0x0

    if-lez p3, :cond_1

    .line 94
    invoke-virtual {p1, p2, v1, p3}, Ljava/io/FileOutputStream;->write([BII)V

    .line 95
    iget-wide v1, p0, Lcom/zystudio/assist/logic/CopyTask;->copySize:J

    int-to-long v3, p3

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/zystudio/assist/logic/CopyTask;->copySize:J

    long-to-float p3, v1

    .line 97
    iget-wide v1, p0, Lcom/zystudio/assist/logic/CopyTask;->totalSize:J

    long-to-float v1, v1

    div-float/2addr p3, v1

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float p3, p3, v1

    .line 98
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    move-result p3

    .line 99
    iget v1, p0, Lcom/zystudio/assist/logic/CopyTask;->copyProcess:I

    if-le p3, v1, :cond_0

    .line 100
    new-instance v1, Lcom/zystudio/assist/logic/CopyTask$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p3}, Lcom/zystudio/assist/logic/CopyTask$$ExternalSyntheticLambda2;-><init>(Lcom/zystudio/assist/logic/CopyTask;I)V

    invoke-virtual {p0, v1}, Lcom/zystudio/assist/logic/CopyTask;->runOnUI(Ljava/lang/Runnable;)V

    .line 101
    iput p3, p0, Lcom/zystudio/assist/logic/CopyTask;->copyProcess:I

    goto :goto_0

    .line 104
    :cond_1
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->flush()V

    const/4 p2, 0x2

    new-array p2, p2, [Ljava/io/Closeable;

    aput-object p1, p2, v1

    const/4 p1, 0x1

    aput-object v0, p2, p1

    .line 105
    invoke-static {p2}, Lcom/zystudio/assist/tools/ComUtil;->close([Ljava/io/Closeable;)V

    return-void
.end method

.method private launchCopyTask()V
    .locals 4

    .line 51
    invoke-static {}, Lcom/zystudio/assist/logic/ObbSDK;->getInstance()Lcom/zystudio/assist/logic/ObbSDK;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zystudio/assist/logic/ObbSDK;->availableObbFiles()Ljava/util/List;

    move-result-object v0

    .line 52
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_0

    .line 54
    new-instance v0, Lcom/zystudio/assist/logic/CopyTask$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0}, Lcom/zystudio/assist/logic/CopyTask$$ExternalSyntheticLambda3;-><init>(Lcom/zystudio/assist/logic/CopyTask;)V

    invoke-virtual {p0, v0}, Lcom/zystudio/assist/logic/CopyTask;->runOnUI(Ljava/lang/Runnable;)V

    return-void

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    .line 60
    :try_start_0
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {p0, v2, v1, v3}, Lcom/zystudio/assist/logic/CopyTask;->copyLogic(IILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    .line 62
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    const-string v0, ""

    .line 66
    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 67
    iget-object v0, p0, Lcom/zystudio/assist/logic/CopyTask;->copyState:Lcom/zystudio/assist/interf/ICopyState;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/zystudio/assist/logic/CopyTask$$ExternalSyntheticLambda4;

    invoke-direct {v1, v0}, Lcom/zystudio/assist/logic/CopyTask$$ExternalSyntheticLambda4;-><init>(Lcom/zystudio/assist/interf/ICopyState;)V

    invoke-virtual {p0, v1}, Lcom/zystudio/assist/logic/CopyTask;->runOnUI(Ljava/lang/Runnable;)V

    goto :goto_2

    .line 70
    :cond_2
    new-instance v1, Lcom/zystudio/assist/logic/CopyTask$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0, v0}, Lcom/zystudio/assist/logic/CopyTask$$ExternalSyntheticLambda5;-><init>(Lcom/zystudio/assist/logic/CopyTask;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/zystudio/assist/logic/CopyTask;->runOnUI(Ljava/lang/Runnable;)V

    :goto_2
    return-void
.end method


# virtual methods
.method public execute()V
    .locals 2

    .line 39
    invoke-static {}, Lcom/zystudio/assist/logic/ObbSDK;->getInstance()Lcom/zystudio/assist/logic/ObbSDK;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zystudio/assist/logic/ObbSDK;->getTotalSize()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/zystudio/assist/logic/CopyTask;->totalSize:J

    .line 40
    iget-object v0, p0, Lcom/zystudio/assist/logic/CopyTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/zystudio/assist/logic/CopyTask$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/zystudio/assist/logic/CopyTask$$ExternalSyntheticLambda0;-><init>(Lcom/zystudio/assist/logic/CopyTask;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method synthetic lambda$copyLogic$2$com-zystudio-assist-logic-CopyTask(IIJ)V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/zystudio/assist/logic/CopyTask;->copyState:Lcom/zystudio/assist/interf/ICopyState;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/zystudio/assist/interf/ICopyState;->onCopyStart(IIJ)V

    return-void
.end method

.method synthetic lambda$copyLogic$3$com-zystudio-assist-logic-CopyTask(I)V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/zystudio/assist/logic/CopyTask;->copyState:Lcom/zystudio/assist/interf/ICopyState;

    invoke-interface {v0, p1}, Lcom/zystudio/assist/interf/ICopyState;->onCopying(I)V

    return-void
.end method

.method synthetic lambda$launchCopyTask$0$com-zystudio-assist-logic-CopyTask()V
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/zystudio/assist/logic/CopyTask;->copyState:Lcom/zystudio/assist/interf/ICopyState;

    const-string v1, "\u672a\u627e\u5230\u6e38\u620f\u8d44\u6e90\uff0c\u8bf7\u91cd\u8bd5\u3002"

    invoke-interface {v0, v1}, Lcom/zystudio/assist/interf/ICopyState;->onCopyFail(Ljava/lang/String;)V

    return-void
.end method

.method synthetic lambda$launchCopyTask$1$com-zystudio-assist-logic-CopyTask(Ljava/lang/String;)V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/zystudio/assist/logic/CopyTask;->copyState:Lcom/zystudio/assist/interf/ICopyState;

    invoke-interface {v0, p1}, Lcom/zystudio/assist/interf/ICopyState;->onCopyFail(Ljava/lang/String;)V

    return-void
.end method

.method public runOnUI(Ljava/lang/Runnable;)V
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/zystudio/assist/logic/CopyTask;->handler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public shutdown()V
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/zystudio/assist/logic/CopyTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    return-void
.end method
