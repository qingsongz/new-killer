.class public Lcom/zystudio/assist/ui/ObbView;
.super Landroid/widget/RelativeLayout;
.source "ObbView.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private iconIV:Landroid/widget/ImageView;

.field private txtDescription:Landroid/widget/TextView;

.field private txtGameName:Landroid/widget/TextView;

.field private txtProgress:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 32
    iput-object p1, p0, Lcom/zystudio/assist/ui/ObbView;->context:Landroid/content/Context;

    const-string p1, "#464765"

    .line 33
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/zystudio/assist/ui/ObbView;->setBackgroundColor(I)V

    .line 34
    invoke-direct {p0}, Lcom/zystudio/assist/ui/ObbView;->initUI()V

    .line 35
    invoke-direct {p0}, Lcom/zystudio/assist/ui/ObbView;->setDefaultUI()V

    return-void
.end method

.method private addAppICON()V
    .locals 4

    .line 87
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/zystudio/assist/ui/ObbView;->iconIV:Landroid/widget/ImageView;

    .line 88
    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setId(I)V

    .line 89
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->context:Landroid/content/Context;

    const/high16 v2, 0x42ae0000    # 87.0f

    .line 90
    invoke-static {v1, v2}, Lcom/zystudio/assist/tools/DensityUtil;->dp2px(Landroid/content/Context;F)I

    move-result v1

    iget-object v3, p0, Lcom/zystudio/assist/ui/ObbView;->context:Landroid/content/Context;

    .line 91
    invoke-static {v3, v2}, Lcom/zystudio/assist/tools/DensityUtil;->dp2px(Landroid/content/Context;F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xe

    .line 92
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xa

    const/4 v2, -0x1

    .line 93
    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 94
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/zystudio/assist/ui/ObbView;->isHorizontal()Z

    move-result v2

    if-eqz v2, :cond_0

    const/high16 v2, 0x41a00000    # 20.0f

    goto :goto_0

    :cond_0
    const/high16 v2, 0x42a00000    # 80.0f

    :goto_0
    invoke-static {v1, v2}, Lcom/zystudio/assist/tools/DensityUtil;->dp2px(Landroid/content/Context;F)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 95
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->iconIV:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 96
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->iconIV:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, v0}, Lcom/zystudio/assist/ui/ObbView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private addAppName()V
    .locals 4

    .line 100
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/zystudio/assist/ui/ObbView;->txtGameName:Landroid/widget/TextView;

    .line 101
    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 102
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xe

    .line 103
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 104
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->iconIV:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 105
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/zystudio/assist/ui/ObbView;->isHorizontal()Z

    move-result v2

    if-eqz v2, :cond_0

    const/high16 v2, 0x41200000    # 10.0f

    goto :goto_0

    :cond_0
    const/high16 v2, 0x41f00000    # 30.0f

    :goto_0
    invoke-static {v1, v2}, Lcom/zystudio/assist/tools/DensityUtil;->dp2px(Landroid/content/Context;F)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 106
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->txtGameName:Landroid/widget/TextView;

    const-string v2, "#333333"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 107
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->txtGameName:Landroid/widget/TextView;

    const/4 v2, 0x1

    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 108
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->txtGameName:Landroid/widget/TextView;

    invoke-virtual {p0, v1, v0}, Lcom/zystudio/assist/ui/ObbView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private addDescText()V
    .locals 4

    .line 131
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/zystudio/assist/ui/ObbView;->txtDescription:Landroid/widget/TextView;

    .line 132
    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 133
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 134
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->txtGameName:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getId()I

    move-result v1

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 135
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/zystudio/assist/ui/ObbView;->isHorizontal()Z

    move-result v2

    if-eqz v2, :cond_0

    const/high16 v2, 0x41000000    # 8.0f

    goto :goto_0

    :cond_0
    const/high16 v2, 0x41a00000    # 20.0f

    :goto_0
    invoke-static {v1, v2}, Lcom/zystudio/assist/tools/DensityUtil;->dp2px(Landroid/content/Context;F)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 136
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->context:Landroid/content/Context;

    const/high16 v2, 0x41800000    # 16.0f

    invoke-static {v1, v2}, Lcom/zystudio/assist/tools/DensityUtil;->dp2px(Landroid/content/Context;F)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 137
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->context:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/zystudio/assist/tools/DensityUtil;->dp2px(Landroid/content/Context;F)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 138
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->txtDescription:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 139
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->txtDescription:Landroid/widget/TextView;

    const-string v2, "#5e6d81"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 140
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->txtDescription:Landroid/widget/TextView;

    const/4 v2, 0x1

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 141
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->txtDescription:Landroid/widget/TextView;

    invoke-virtual {p0, v1, v0}, Lcom/zystudio/assist/ui/ObbView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private addHintText()V
    .locals 4

    .line 112
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 113
    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 114
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xe

    .line 115
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v2, 0xc

    const/4 v3, -0x1

    .line 116
    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 117
    iget-object v2, p0, Lcom/zystudio/assist/ui/ObbView;->context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/zystudio/assist/ui/ObbView;->isHorizontal()Z

    move-result v3

    if-eqz v3, :cond_0

    const/high16 v3, 0x41a00000    # 20.0f

    goto :goto_0

    :cond_0
    const/high16 v3, 0x42200000    # 40.0f

    :goto_0
    invoke-static {v2, v3}, Lcom/zystudio/assist/tools/DensityUtil;->dp2px(Landroid/content/Context;F)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    const-string v2, "#006400"

    .line 118
    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v2, 0x1

    const/high16 v3, 0x41600000    # 14.0f

    .line 119
    invoke-virtual {v0, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 121
    invoke-direct {p0}, Lcom/zystudio/assist/ui/ObbView;->isHorizontal()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "\u8d44\u6e90\u51c6\u5907\u4e2d\uff0c\u8bf7\u7a0d\u5019...\t\t\uff08\u6b64\u8fc7\u7a0b\u4e0d\u6d88\u8017\u6d41\u91cf\uff09"

    goto :goto_1

    :cond_1
    const-string v2, "\u8d44\u6e90\u51c6\u5907\u4e2d\uff0c\u8bf7\u7a0d\u5019...\n\n\uff08\u6b64\u8fc7\u7a0b\u4e0d\u6d88\u8017\u6d41\u91cf\uff09"

    .line 126
    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    invoke-virtual {p0, v0, v1}, Lcom/zystudio/assist/ui/ObbView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private addProgressText()V
    .locals 4

    .line 145
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/zystudio/assist/ui/ObbView;->txtProgress:Landroid/widget/TextView;

    .line 146
    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 147
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 148
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->txtDescription:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getId()I

    move-result v1

    const/4 v3, 0x3

    invoke-virtual {v0, v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 149
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/zystudio/assist/ui/ObbView;->isHorizontal()Z

    move-result v3

    if-eqz v3, :cond_0

    const/high16 v3, 0x41000000    # 8.0f

    goto :goto_0

    :cond_0
    const/high16 v3, 0x42180000    # 38.0f

    :goto_0
    invoke-static {v1, v3}, Lcom/zystudio/assist/tools/DensityUtil;->dp2px(Landroid/content/Context;F)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    const/16 v1, 0xe

    .line 150
    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 151
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->txtProgress:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 152
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->txtProgress:Landroid/widget/TextView;

    const-string v2, "#5e6d81"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 153
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->txtProgress:Landroid/widget/TextView;

    const/4 v2, 0x1

    const/high16 v3, 0x42340000    # 45.0f

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 154
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->txtProgress:Landroid/widget/TextView;

    invoke-virtual {p0, v1, v0}, Lcom/zystudio/assist/ui/ObbView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private addWhiteZone()V
    .locals 5

    .line 76
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 77
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v3, 0xa

    .line 80
    invoke-virtual {v1, v3, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 81
    iget-object v3, p0, Lcom/zystudio/assist/ui/ObbView;->context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/zystudio/assist/ui/ObbView;->isHorizontal()Z

    move-result v4

    if-eqz v4, :cond_0

    const/high16 v4, 0x42700000    # 60.0f

    goto :goto_0

    :cond_0
    const/high16 v4, 0x42f00000    # 120.0f

    :goto_0
    invoke-static {v3, v4}, Lcom/zystudio/assist/tools/DensityUtil;->dp2px(Landroid/content/Context;F)I

    move-result v3

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 82
    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 83
    invoke-virtual {p0, v0, v1}, Lcom/zystudio/assist/ui/ObbView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private initUI()V
    .locals 0

    .line 61
    invoke-direct {p0}, Lcom/zystudio/assist/ui/ObbView;->addWhiteZone()V

    .line 63
    invoke-direct {p0}, Lcom/zystudio/assist/ui/ObbView;->addAppICON()V

    .line 65
    invoke-direct {p0}, Lcom/zystudio/assist/ui/ObbView;->addAppName()V

    .line 67
    invoke-direct {p0}, Lcom/zystudio/assist/ui/ObbView;->addHintText()V

    .line 69
    invoke-direct {p0}, Lcom/zystudio/assist/ui/ObbView;->addDescText()V

    .line 71
    invoke-direct {p0}, Lcom/zystudio/assist/ui/ObbView;->addProgressText()V

    return-void
.end method

.method private isHorizontal()Z
    .locals 2

    .line 172
    iget-object v0, p0, Lcom/zystudio/assist/ui/ObbView;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private setDefaultUI()V
    .locals 3

    const-string v0, " "

    .line 158
    invoke-virtual {p0, v0}, Lcom/zystudio/assist/ui/ObbView;->showBigHint(Ljava/lang/String;)V

    .line 159
    iget-object v0, p0, Lcom/zystudio/assist/ui/ObbView;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/zystudio/assist/tools/ComUtil;->parseApkInfo(Landroid/content/Context;)Lcom/zystudio/assist/interf/GameInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 161
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->iconIV:Landroid/widget/ImageView;

    iget-object v2, v0, Lcom/zystudio/assist/interf/GameInfo;->gameIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 162
    iget-object v1, p0, Lcom/zystudio/assist/ui/ObbView;->txtGameName:Landroid/widget/TextView;

    iget-object v0, v0, Lcom/zystudio/assist/interf/GameInfo;->gameLabel:Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public showBigHint(Ljava/lang/String;)V
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/zystudio/assist/ui/ObbView;->txtProgress:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showDesc(Ljava/lang/String;)V
    .locals 2

    .line 51
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    iget-object p1, p0, Lcom/zystudio/assist/ui/ObbView;->txtDescription:Landroid/widget/TextView;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/zystudio/assist/ui/ObbView;->txtDescription:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 55
    iget-object v0, p0, Lcom/zystudio/assist/ui/ObbView;->txtDescription:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method
