.class public final Lcom/zystudio/assist/ObbConfig;
.super Ljava/lang/Object;
.source "ObbConfig.java"


# static fields
.field private static config:Lcom/zystudio/assist/ObbConfig; = null

.field private static gameEntry:Ljava/lang/String; = "test.boot.TestActivity"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get()Lcom/zystudio/assist/ObbConfig;
    .locals 1

    .line 12
    sget-object v0, Lcom/zystudio/assist/ObbConfig;->config:Lcom/zystudio/assist/ObbConfig;

    if-nez v0, :cond_0

    .line 13
    new-instance v0, Lcom/zystudio/assist/ObbConfig;

    invoke-direct {v0}, Lcom/zystudio/assist/ObbConfig;-><init>()V

    sput-object v0, Lcom/zystudio/assist/ObbConfig;->config:Lcom/zystudio/assist/ObbConfig;

    .line 15
    :cond_0
    sget-object v0, Lcom/zystudio/assist/ObbConfig;->config:Lcom/zystudio/assist/ObbConfig;

    return-object v0
.end method


# virtual methods
.method public gameLauncherName(Z)Ljava/lang/String;
    .locals 0

    if-eqz p1, :cond_0

    const-string p1, "com.zystudio.dev.ZyActivity"

    goto :goto_0

    .line 38
    :cond_0
    sget-object p1, Lcom/zystudio/assist/ObbConfig;->gameEntry:Ljava/lang/String;

    :goto_0
    return-object p1
.end method

.method public obbAbsolutePath()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
