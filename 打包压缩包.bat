@echo off

REM 常量：压缩软件的目录
set BzPath="D:\tool\Bandizip\bz.exe"

REM 自定义：输入和输出目录
set AkPath=%~dp0
REM 获取当前日期，然后切片得到日期
REM set CurrentDate=%DATE:~5,2%%DATE:~8,2%
REM 暂时不使用这个日期名
set ZipPath=%AkPath%\ZyKiller.zip


REM 待压缩的文件夹
set dir1=%AkPath%\bin
set dir2=%AkPath%\cfgs
set dir3=%AkPath%\extension
set dir4=%AkPath%\plugins

REM 待压缩的文件
set file1=%AkPath%\AKCommon.bpl
set file2=%AkPath%\AndroidKiller.exe
set file3=%AkPath%\configs.ini
set file4=%AkPath%\lua5.3.0.dll
set file5=%AkPath%\rtl230.bpl
set file6=%AkPath%\vcl230.bpl
set file7=%AkPath%\适配插件.bat


REM 清除旧的压缩包
if exist %ZipPath% (
    del /F %ZipPath%
)

REM 开始压缩：指定的目录 和 指定的文件夹
%BzPath% a %ZipPath% %dir1% %dir2% %dir3% %dir4% %file1% %file2% %file3% %file4% %file5% %file6% %file7%